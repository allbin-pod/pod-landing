module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    browserSync: {
        dev: {
            bsFiles: {
                src : [
                    'src/assets/css/*.css',
                    'src/*.html'
                ]
            },
            options: {
                watchTask: true,
                server: './src'
            }
        }
    },
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
            "src/assets/css/main.css": "src/assets/less/*.less"
        }
      }
    },
    copy: {
      main: {
          files: [
              {src: 'src/index.html', dest: 'dist/index.html'},
              {expand: true, cwd: 'src/assets/img/', src: ['**'], dest: 'dist/assets/img/'},
              {expand: true, cwd: 'node_modules/bootstrap/dist/css/', src: ['bootstrap.min.css'], dest: 'src/assets/css/'},
              {expand: true, cwd: 'node_modules/jquery/dist/', src: ['jquery.min.js'], dest: 'src/assets/js/'},
              {expand: true, cwd: 'node_modules/css-reset/', src: ['reset.css'], dest: 'src/assets/css/'},
              {expand: true, cwd: 'node_modules/sweetalert/dist/', src: ['sweetalert.css'], dest: 'src/assets/css/'},
              {expand: true, cwd: 'node_modules/sweetalert/dist/', src: ['sweetalert.min.js'], dest: 'src/assets/js/'}
          ]
      },
    },
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: ['src/js/*.js'],
        dest: 'dist/assets/js/<%= pkg.name %>.js'
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          'dist/assets/js/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
        }
      }
    },
    jshint: {
      files: ['Gruntfile.js', 'src/js/*.js'],
      options: {
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true
        }
      }
    },
    watch: {
      files: ['src/assets/less/*'],
      tasks: ['less']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-less');

  grunt.registerTask('default', ['jshint', 'concat', 'uglify', 'copy']); // also copy/contrib main css

  grunt.registerTask('watch_less', ['browserSync', 'watch']);

};

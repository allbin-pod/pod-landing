# POD-Landing
Sales page for POD©

npm install
grunt watch_less
grunt release (not implemented)

# devDependencies
- bootstrap: ^3.3.6,
- css-reset: 0.0.1,
- grunt: ^0.4.5,
- grunt-browser-sync: ^2.2.0,
- grunt-browserify: ^4.0.1,
- grunt-bump: ^0.6.0,
- grunt-contrib-concat: ^0.5.1,
- grunt-contrib-copy: ^0.8.2,
- grunt-contrib-jshint: ^0.11.3,
- grunt-contrib-less: ^1.1.0,
- grunt-contrib-uglify: ^0.9.2,
- grunt-contrib-watch: ^0.6.1,
- jquery: ^2.1.4,
- less: ^2.5.3
